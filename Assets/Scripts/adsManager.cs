﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Analytics;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class adsManager : MonoBehaviour, IUnityAdsListener
{
    public static adsManager Instance;
    [Space]
    [Space]
    [Header("Developer : Awais")]
    [Header("Update 26 Nov 2020 ")]
    [Space]
    public string AdmobAppID;
    public AdPosition BannerAdPosition;
    public string BannerAdID;
    public string RectBannerAdID;
    public string interstitial;
    public string SP_Interstitial;
    public string RewardedVideoAdID;
    public string unityId;
    [Space]
    // public string NativeAdvanceAdID;
    [Space]
    public bool IsShowTestAds;
    public bool ShowDebugLogs;
    bool isVideoCompleted = false;
    [HideInInspector] public bool isBannerAdLoaded = false;
    [HideInInspector] public bool isRectBannerAdLoaded = false;
    [HideInInspector] public bool isRectBannerAdVisible, isBannerVisible = true;
    [HideInInspector] public bool isRewardedVideoAdLoaded = false;

    public BannerView myBannerView, BannerAdViewBottom, tempBannerView;
    private InterstitialAd myInterstitialAd;//fail complete
    private InterstitialAd myInterstitialAd2;//splash


    //public List<string> deviceIds;
    private RewardBasedVideoAd rewardBasedVideo;


    private BannerView myRectBannerView, tempRectBannerView;

    //public GameObject topbannerbackground, topLeftbannerBackground, topRightBannerbackground, bottomBannerBackground, bottomLeftBannerBackground, BottomRightBannerBackground, rectbannerBackground;
    public int Option;


    [Space]
    [Tooltip("Either use this \"removeads\" key or put your own here. Dont forget to set the value of that key 1 on succesful purchase of remove Ads")]
    public string removeAds_InAPPKey = "Removeads";

    public static bool gameStartedLogged = false;

    void Awake()
    {

        /// <summary>
        /// Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else
            if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);


        if (IsShowTestAds)
        {
            AdmobAppID = "ca-app-pub-5383330200914402~4441668857";
            BannerAdID = "ca-app-pub-3940256099942544/6300978111";
            RectBannerAdID = "ca-app-pub-3940256099942544/6300978111";
            interstitial = "ca-app-pub-3940256099942544/1033173712";
            SP_Interstitial = "ca-app-pub-3940256099942544/1033173712";
            RewardedVideoAdID = "ca-app-pub-3940256099942544/5224354917";
            // NativeAdvanceAdID = "ca-app-pub-3940256099942544/2247696110";
        }


        MobileAds.Initialize(initstatus=>{ });


    }



    public void Start()
    {


        if (!gameStartedLogged)
        {

            gameStartedLogged = true;

        }
        intitADs();
    }


    public void intitADs()
    {
        StartCoroutine(initializeFullScreenAds());
    }
    public IEnumerator initializeFullScreenAds()
    {

        yield return new WaitForSecondsRealtime(1f);
       // RequestSplashInterstitialAd();
        RequestInterstitialAd2();
        
        RequestBannerAd(AdPosition.Bottom);
        RequestRewardedVideoAd();
       // RequestRectBannerAd();
        isBannerVisible = false;
        isRectBannerAdVisible = false;
        isInterstitialVisible = false;

        Advertisement.Initialize(unityId, IsShowTestAds);
        Advertisement.AddListener(this);

    }
    //***************************** MediationTestSuite *****************************************************************************



    //***************************** Banner Ad *****************************************************************************
    #region Banner Ads 
    // Admob banner Ads fuction
    public void RequestBannerAd(AdPosition bannerAdPositionLocal)
    {
        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {
            //#if !UNITY_EDITOR



            BannerAdPosition = bannerAdPositionLocal;        //********
            this.myBannerView = new BannerView(BannerAdID, AdSize.Banner, BannerAdPosition);
            InItBannerEvents();
            AdRequest request = new AdRequest.Builder().Build();
            this.myBannerView.LoadAd(request);
            myBannerView.Hide();
            //#endif

        }
    }


    public void ShowBannerAd()
    {
        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {
            isBannerVisible = true;

            myBannerView.SetPosition(AdPosition.Top);
            myBannerView.Show();

        }
    }

    public void HideBannerAd()
    {
        isBannerVisible = false;
        if (myBannerView != null)
        {
            myBannerView.Hide();

        }
    }

    public void ShowBannerBottom()
    {
        isBannerVisible = true;
        myBannerView.SetPosition(AdPosition.Bottom);
        myBannerView.Show();
    } public void ShowBannerTopLeft()
    {
        isBannerVisible = true;
        myBannerView.SetPosition(AdPosition.TopLeft);
        myBannerView.Show();
    }
    #endregion


    //*****************************Rect Banner Ad *****************************************************************************
    #region React Banner Ads 
    // Admob banner Ads fuction
    public void RequestRectBannerAd()
    {
        Debug.Log("rect banner ad requested...");
        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {




            isRectBannerAdVisible = true;
            AdSize adSizemiddleleft = new AdSize(300, 250);

            //middle left           
            //  int placeY = (int)((Screen.width / 2) * 0.055);
            //  int placeX = 0;
            //    this.myRectBannerView = new BannerView(RectBannerAdID, adSizemiddleleft, placeX, placeY);
            this.myRectBannerView = new BannerView(RectBannerAdID, adSizemiddleleft, AdPosition.BottomLeft);

            //middle Right
            //int placeY = (int)((Screen.width / 2) * 0.055);
            //int placeX = (int)(Screen.width - 300);
            //this.myRectBannerView = new BannerView(RectBannerAdID, adSizemiddleleft, placeX, placeY);


            InItRectBannerEvents();
            AdRequest request = new AdRequest.Builder().Build();
            this.myRectBannerView.LoadAd(request);

            myRectBannerView.Hide();

            //HideRectBannerAd();
        }
    }

    public void ShowRectBannerAd()
    {

        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {

            myRectBannerView.Show();



        }
    }
    public void HideRectBannerAd()
    {
        isRectBannerAdVisible = false;
        if (myRectBannerView != null)
        {

            myRectBannerView.Hide();

        }
    }

    #endregion

    //***************************** Interstitial Ad *****************************************************************************
    #region InterstitialAd
    public void RequestSplashInterstitialAd()
    {
        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {

            // Initialize an InterstitialAd.
            this.myInterstitialAd = new InterstitialAd(SP_Interstitial);
            InItInterstitialEvents();
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the interstitial with the request.
            this.myInterstitialAd.LoadAd(request);
        }
    }
    bool isInterstitialVisible = false;
    public void ShowSplashInterstitialAd()
    {
        isInterstitialVisible = true;
        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {

            if (myInterstitialAd.IsLoaded())
            {
                myInterstitialAd.Show();

            }
            

        }
    }



    public void hideInterstitialAd()
    {
        isInterstitialVisible = false;
    }
    public void RequestInterstitialAd2()
    {
        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {
            // Initialize an InterstitialAd.
            this.myInterstitialAd2 = new InterstitialAd(interstitial);
            InItInterstitialEvents2();
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the interstitial with the request.
            this.myInterstitialAd2.LoadAd(request);
        }
    }

    public void ShowInterstitialAd2()
    {
        if (PlayerPrefs.GetInt(removeAds_InAPPKey, 0) == 0)
        {
            if (myInterstitialAd2.IsLoaded())
            {
                myInterstitialAd2.Show();
            }
            
            else
            {
                ShowUnityAd();
            }

        }
        //LogEvent("interstitial_showed");
    }



    #endregion


    //***************************** RewardedVideo Ad *****************************************************************************
    #region RewardedVideoAd


    public void RequestRewardedVideoAd()
    {
        isRewardedVideoAdLoaded = false;

        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        InItrewardVideoEvents();
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, RewardedVideoAdID);

    }








    public void ShowRewardedVideoAd()
    {
        isVideoCompleted = false;

        if (isRewardedVideoAdLoaded)
        {
            rewardBasedVideo.Show();

        }
        else {
            ShowUnityRewardedVideo();
        }



    }

    public void OnRewardedVideoComplete()
    {

        // GIVE YOUR REWARD HERE....

        if (Option == 1)
        {
            CashManager.Instance.CashDebit(50);
        }
        if (Option == 2)
        {
            LevelCompletePanel.instance.GiveDoubleReward();
        }
        if (Option == 3)
        {
            GameManger.instance.AddNewFlask();
        }


    }

    public void OnRewardedVideoSkipped()
    {
        //  ShowToast.showToastMessage("On Rewarded Video Skipped");


    }

    #endregion






    //***************************** Ads Events *****************************************************************************
    #region Events



    #region Banner Events
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {

        if (isBannerVisible)
            myBannerView.Show();   //  showBannerBackground(BannerAdPosition);



        isBannerAdLoaded = true;
        //if(ShowDebugLogs)
        MonoBehaviour.print("HandleAdLoaded event received");

    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //isBannerAdLoaded = false;
        //print("Banner calll back time: " + _FloatValues[0].value);

        //Invoke("RequestBannerAd", _FloatValues[0].value);//_FloatValues[0].value




        //myBannerView.Destroy();

        isBannerAdLoaded = false;
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    #endregion


    #region Interstatial Events
    public void HandleOnInterstatialAdLoaded(object sender, EventArgs args)
    {

        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdLoaded event received");

        //  myInterstitialAd.Show();

    }

    public void HandleOnInterstatialAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("Interstitial failed to load");



        if (ShowDebugLogs)
            MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnInterstatialAdOpened(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
        {
            MonoBehaviour.print("HandleAdOpened event received");
        }


    }

    public void HandleOnInterstatialAdClosed(object sender, EventArgs args)
    {
        hideInterstitialAd();
        // RequestInterstitialAd();
        //LevelManager.instance.BuyEverythingPanekl.SetActive(false);
        //LevelManager.instance.loadingScreen.SetActive(false);
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnInterstatialAdLeavingApplication(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    //InterstitiaL2 EVENTS
    public void HandleOnInterstatialAdLoaded2(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdLoaded event received");
        if (isInterstitialVisible)
        {
            myInterstitialAd2.Show();
        }
    }

    public void HandleOnInterstatialAdFailedToLoad2(object sender, AdFailedToLoadEventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnInterstatialAdOpened2(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
        {
            MonoBehaviour.print("HandleAdOpened event received");
        }


    }

    public void HandleOnInterstatialAdClosed2(object sender, EventArgs args)
    {
        isInterstitialVisible = false;
        RequestInterstitialAd2();
      //  ShowRectBannerAd(); //show react banner after interstitial on settings and win/fail panel
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnInterstatialAdLeavingApplication2(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdLeavingApplication event received");
    }


    #endregion


    #region Rewarded Video Events
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        isRewardedVideoAdLoaded = true;
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        isRewardedVideoAdLoaded = false;
        //RequestRewardedVideoAd();
        Debug.Log("rewarded video failed to load");


        if (ShowDebugLogs)
            MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
        {
            MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
        }


    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleRewardBasedVideoStarted event received");

    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        isRewardedVideoAdLoaded = false;
        RequestRewardedVideoAd();
        if (!isVideoCompleted)
        {
            OnRewardedVideoSkipped();
        }
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleRewardBasedVideoClosed event received");

    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        isVideoCompleted = true;
        OnRewardedVideoComplete();

        if (ShowDebugLogs)
            MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);

    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }
    #endregion


    #region RectBanner Events
    public void HandleOnRectBannerAdLoaded(object sender, EventArgs args)
    {
        isRectBannerAdLoaded = true;

        MonoBehaviour.print("HandleAdLoaded event received");
        if (isRectBannerAdVisible)
        {
            myRectBannerView.Show();

        }



    }

    public void HandleOnRectBannerAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        isRectBannerAdLoaded = false;
        Debug.Log("rect banner failed to load");

        if (ShowDebugLogs)
            MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnRectBannerAdOpened(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnRectBannerAdClosed(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnRectBannerAdLeavingApplication(object sender, EventArgs args)
    {
        if (ShowDebugLogs)
            MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    #endregion


    void InItBannerEvents()
    {


        // Called when an ad request has successfully loaded.
        this.myBannerView.OnAdLoaded -= HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this.myBannerView.OnAdFailedToLoad -= HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        this.myBannerView.OnAdOpening -= HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        this.myBannerView.OnAdClosed -= HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.myBannerView.OnAdLeavingApplication -= HandleOnAdLeavingApplication;


        // Called when an ad request has successfully loaded.
        this.myBannerView.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this.myBannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        this.myBannerView.OnAdOpening += HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        this.myBannerView.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.myBannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;

    }

    void InItInterstitialEvents()

    {

        // Called when an ad request has successfully loaded.
        this.myInterstitialAd.OnAdLoaded -= HandleOnInterstatialAdLoaded;
        // Called when an ad request failed to load.
        this.myInterstitialAd.OnAdFailedToLoad -= HandleOnInterstatialAdFailedToLoad;
        // Called when an ad is shown.
        this.myInterstitialAd.OnAdOpening -= HandleOnInterstatialAdOpened;
        // Called when the ad is closed.
        this.myInterstitialAd.OnAdClosed -= HandleOnInterstatialAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.myInterstitialAd.OnAdLeavingApplication -= HandleOnInterstatialAdLeavingApplication;


        // Called when an ad request has successfully loaded.
        this.myInterstitialAd.OnAdLoaded += HandleOnInterstatialAdLoaded;
        // Called when an ad request failed to load.
        this.myInterstitialAd.OnAdFailedToLoad += HandleOnInterstatialAdFailedToLoad;
        // Called when an ad is shown.
        this.myInterstitialAd.OnAdOpening += HandleOnInterstatialAdOpened;
        // Called when the ad is closed.
        this.myInterstitialAd.OnAdClosed += HandleOnInterstatialAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.myInterstitialAd.OnAdLeavingApplication += HandleOnInterstatialAdLeavingApplication;

    }

    void InItInterstitialEvents2()

    {

        // Called when an ad request has successfully loaded.
        this.myInterstitialAd2.OnAdLoaded -= HandleOnInterstatialAdLoaded2;
        // Called when an ad request failed to load.
        this.myInterstitialAd2.OnAdFailedToLoad -= HandleOnInterstatialAdFailedToLoad2;
        // Called when an ad is shown.
        this.myInterstitialAd2.OnAdOpening -= HandleOnInterstatialAdOpened2;
        // Called when the ad is closed.
        this.myInterstitialAd2.OnAdClosed -= HandleOnInterstatialAdClosed2;
        // Called when the ad click caused the user to leave the application.
        this.myInterstitialAd2.OnAdLeavingApplication -= HandleOnInterstatialAdLeavingApplication2;


        // Called when an ad request has successfully loaded.
        this.myInterstitialAd2.OnAdLoaded += HandleOnInterstatialAdLoaded2;
        // Called when an ad request failed to load.
        this.myInterstitialAd2.OnAdFailedToLoad += HandleOnInterstatialAdFailedToLoad2;
        // Called when an ad is shown.
        this.myInterstitialAd2.OnAdOpening += HandleOnInterstatialAdOpened2;
        // Called when the ad is closed.
        this.myInterstitialAd2.OnAdClosed += HandleOnInterstatialAdClosed2;
        // Called when the ad click caused the user to leave the application.
        this.myInterstitialAd2.OnAdLeavingApplication += HandleOnInterstatialAdLeavingApplication2;

    }


    void InItrewardVideoEvents()
    {

        // Called when an ad request has successfully loaded.
        this.rewardBasedVideo.OnAdLoaded -= HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        this.rewardBasedVideo.OnAdFailedToLoad -= HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        this.rewardBasedVideo.OnAdOpening -= HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        this.rewardBasedVideo.OnAdStarted -= HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        this.rewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        this.rewardBasedVideo.OnAdClosed -= HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        this.rewardBasedVideo.OnAdLeavingApplication -= HandleRewardBasedVideoLeftApplication;


        // Called when an ad request has successfully loaded.
        this.rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        this.rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        this.rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        this.rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        this.rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        this.rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        this.rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

    }


    void InItRectBannerEvents()
    {

        // Called when an ad request has successfully loaded.
        this.myRectBannerView.OnAdLoaded -= HandleOnRectBannerAdLoaded;
        // Called when an ad request failed to load.
        this.myRectBannerView.OnAdFailedToLoad -= HandleOnRectBannerAdFailedToLoad;
        // Called when an ad is clicked.
        this.myRectBannerView.OnAdOpening -= HandleOnRectBannerAdOpened;
        // Called when the user returned from the app after an ad click.
        this.myRectBannerView.OnAdClosed -= HandleOnRectBannerAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.myRectBannerView.OnAdLeavingApplication -= HandleOnRectBannerAdLeavingApplication;


        // Called when an ad request has successfully loaded.
        this.myRectBannerView.OnAdLoaded += HandleOnRectBannerAdLoaded;
        // Called when an ad request failed to load.
        this.myRectBannerView.OnAdFailedToLoad += HandleOnRectBannerAdFailedToLoad;
        // Called when an ad is clicked.
        this.myRectBannerView.OnAdOpening += HandleOnRectBannerAdOpened;
        // Called when the user returned from the app after an ad click.
        this.myRectBannerView.OnAdClosed += HandleOnRectBannerAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.myRectBannerView.OnAdLeavingApplication += HandleOnRectBannerAdLeavingApplication;

    }

    #endregion



    #region UNITY ADs


    public void ShowUnityRewardedVideo()
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Advertisement.Show("rewardedVideo");
        }
        else
        {
            Debug.Log("Rewarded video is not ready at the moment! Please try again later!");
            RequestRewardedVideoAd();
          ///  ShowRewardedVideoAd();
        }
    }

    public void ShowUnityAd()
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
        Debug.Log("Rewarded video is not ready at the moment! Please try again later!");

    }
    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            Debug.Log("Give  unity reward here");
            OnRewardedVideoComplete();

        }
        else if (showResult == ShowResult.Skipped)
        {
            Debug.Log("unity ads skipped");

            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {

            Debug.LogWarning("The unity  ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, show the ad:
        if (placementId == "rewardedVideo")
        {
            // Optional actions to take when the placement becomes ready(For example, enable the rewarded ads button)
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }



    public void LogEvent(string message)
    {
        ///Analytics.CustomEvent(message);
        ///
        Firebase.Analytics.FirebaseAnalytics.LogEvent(message);
    }
    #endregion
}






