﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CashManager : MonoBehaviour
{
    public static CashManager Instance;
    public bool giveFreeCash;
    public Text[] cashTxt;
    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance);
            Instance = this;
        }

        UpdateCash();
        // DontDestroyOnLoad(Instance);
        if (giveFreeCash)
            CashDebit(20000);
    }

    public void CashDebit(int amount)
    {
        PlayerPrefs.SetInt("cash", PlayerPrefs.GetInt("cash") + amount);
      
        UpdateCash();
    }
    void UpdateCash()
    {
        int currentCash = CheckCash();
        for (int i = 0; i <= cashTxt.Length - 1; i++)
        {
            cashTxt[i].text = currentCash.ToString();
        }
    }
    public void CreditCash(int amount)
    {
        if (CheckCash() > amount)
        {
            PlayerPrefs.SetInt("cash", PlayerPrefs.GetInt("cash") - amount);
            PlayerPrefs.Save();
        }
        else
        {
            // MainMenu.Instance.ShowMessage("Not have enough cash");
        }
        //   MainMenu.Instance.update_level_locked_status();
        UpdateCash();
    }
    public int CheckCash()
    {
        return PlayerPrefs.GetInt("cash", 0);
    }
}
