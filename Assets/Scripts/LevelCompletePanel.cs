﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class LevelCompletePanel : MonoBehaviour
{
    public static LevelCompletePanel instance;
    public GameObject arrowIndicator;
    public Slider randomRewardSlider;
    public float moveSpeed = 0.2f;
    private int rewardMultiplayer;
    public Text rewardText;
    public Text orignalRewardTxt;
    public Text bonusrewardText; //reward from video ad
    public int originalRewardAmount;
    public GameObject randomRewardbtn;
    public GameObject continueRewardbtn;
    public GameObject collectDoubleRewardbtn;
    public Image[] stars;
    public Sprite filledStar, unfilledStar;
    [HideInInspector] public int starCounts = 2;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        orignalRewardTxt.text = originalRewardAmount.ToString();
        GiveStar();
    }
    bool forward = true;
    bool stop = false;
    private void Update()
    {
        if (stop)
            return;
        if (randomRewardSlider.value < 0.9f && forward)
        {
            randomRewardSlider.value += moveSpeed * Time.deltaTime;
            if (randomRewardSlider.value >= 0.9f)
            {
                forward = false;
            }
        }
        else
        {
            randomRewardSlider.value -= moveSpeed * Time.deltaTime;
            if (randomRewardSlider.value <= .1f)
            {
                forward = true;
            }
        }
        RandomReward(false);
    }
    bool once = false;
    public void RandomReward(bool _Stope)
    {
        float _value = randomRewardSlider.value;
        stop = _Stope;
        /*if (stop)
        {
            randomRewardbtn.SetActive(false);
            continueRewardbtn.SetActive(false);
          
        }*/
        if (once && _Stope)
            return;

        if (_value >= 0 && _value <= 0.18f) //2x reward
        {
            rewardMultiplayer = 2;
        }
        else if (_value > 0.18f && _value <= 0.44f) //3x reward
        {
            rewardMultiplayer = 3;
        }
        else if (_value > 0.44f && _value <= 0.6f) //5x reward
        {
            rewardMultiplayer = 5;
        }
        else if (_value > 0.6f && _value <= 0.82f) //3x reward
        {
            rewardMultiplayer = 3;
        }
        else if (_value > 0.6f && _value <= 0.82f) //2x reward
        {
            rewardMultiplayer = 2;
        }

        rewardText.text = "" + originalRewardAmount * rewardMultiplayer;
        bonusrewardText.text = "" + originalRewardAmount * rewardMultiplayer;
        if (adsManager.Instance && !once && stop)
        {
            once = true;
            adsManager.Instance.Option = 2;
            adsManager.Instance.ShowRewardedVideoAd();
#if UNITY_EDITOR
            GiveDoubleReward();
#endif
        }
    }
    public void GiveDoubleReward()
    {
        randomRewardbtn.SetActive(false);
        continueRewardbtn.SetActive(false);
        collectDoubleRewardbtn.SetActive(true);
       
    }

    public void CollectReward()
    {
        CashManager.Instance.CashDebit(originalRewardAmount * rewardMultiplayer);
        GameManger.instance.NextLevel();
    }
    void GiveStar()
    {
        for (int i = 0; i <= stars.Length - 1; i++)
        {
            if (i <= starCounts)
            {
                stars[i].sprite = filledStar;
            }
            else
            {
                stars[i].sprite = unfilledStar;
            }
        }
    }
}
