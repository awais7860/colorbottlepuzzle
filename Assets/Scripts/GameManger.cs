﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.NiceVibrations;
using DG.Tweening;
using UnityEngine.UI;
/*[ExecuteInEditMode]*/
public class GameManger : MonoBehaviour
{
    public Flask firstFlask, secondFlask;
    public static GameManger instance;
    public ColorUtility color;
    public int completedFlask;
    public int flaskToComplete = 0;
    public bool islevelComplete;
    public GameObject levelCompletebar;
    public GameObject levelCompletePanel;
    public AudioSource waterPouringAS;
    public AudioSource shakesoundAS;
    public GameObject levelCompleteParticles;
    public GameObject bottleCompletePaticles;
    public GameObject waterpouringImage;
    public Vector2 waterpouringOffset;
    public Vector3 offSet; //pouring bottle offset
    public GameObject[] levels;
    private LevelSettings currentLevelComponent;
    public bool TestLevel;
    public int currentLevel;
    public static int levelMoves;
    public Image[] keys;
    public Sprite emptyKey, filledKey;
    public GameObject movesContainer;
    public Text movesCounterTxt;
    public Text thirdStarText;
    public Text secStarText;
    public Text firstdStarText;
    public Text levelNoTxt;
    public GameObject timeContainer;
    public GameObject timemanager;
    public GameObject instructionTxtObj;

    public Image[] stars;
    int thirdstar, secondstar, onestar;
    public Sprite filledStar, unfilledStar;
    [HideInInspector] public int starsCount = 2;
    public enum GameType { Movebase, timebase, both }
    public GameType gameType;
    bool moveDone;

    //custom define color for bottles that 
    public Color purple, pink, silver,
        olive, aqua, teel, lime, marron, orange, brown;

    public const string MAX_UNLOCKED_LEVELS = "maxunlockedlevel";
    public const string CURRENT_LEVEL = "currentlevel";
    public Button addflaskBtn;
    private void Awake()
    {

        if (TestLevel)
        {
            PlayerPrefs.SetInt("currentlevel", currentLevel);
            PlayerPrefs.SetInt("currentmode", (int)gameType);
        }
        completedFlask = 0;
        instance = this;
        UpdateKey();
    }
    public void TapToStart()
    {

        if (gameType == GameType.Movebase)
        {
            levelMoves = 0;//currentLevelComponent.totalMoves;
            movesCounterTxt.text = levelMoves.ToString();
            movesContainer.SetActive(true);
        }
        else if (gameType == GameType.timebase)
        {
            timemanager.GetComponent<TimeManager>().Timer = currentLevelComponent.levelTime;
            timemanager.SetActive(true);
            timeContainer.SetActive(true);
        }
    }
    void UpdateKey()
    {
        for (int i = 0; i <= keys.Length - 1; i++)
        {
            if (i <= PlayerPrefs.GetInt("keys", -1))
            {
                keys[i].sprite = filledKey;
            }
            else
            {
                keys[i].sprite = emptyKey;

            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        levelCompleteParticles.SetActive(false);
        ActivateCurrentLevel();
        UpdateStarPrice(); //star value in star text  //no of moves
        levelNoTxt.text = currentLevel.ToString("");
    }
    void ActivateCurrentLevel()  //active current level on game play start
    {
        currentLevel = PlayerPrefs.GetInt("currentlevel", 0);

        if (currentLevel > levels.Length - 1)
        {
            currentLevel = Random.Range(12, levels.Length - 1);
        }

        for (int i = 0; i <= levels.Length - 1; i++)
        {
            if (i == currentLevel)
            {

                currentLevelComponent = levels[currentLevel].GetComponent<LevelSettings>();
                levels[i].SetActive(true);
            }
            else
            {
                levels[i].SetActive(false);
            }
        }

        flaskToComplete = currentLevelComponent.bottleTofill; //set target for this level
        //levelMoves = currentLevelComponent.totalMoves; //set target for this level
        if (adsManager.Instance)
            adsManager.Instance.LogEvent("level_start" + currentLevel);
    }

    public void CheckLevelComplete()
    {
        if (completedFlask >= flaskToComplete && !islevelComplete)
        {
            islevelComplete = true;
            levelCompleteParticles.SetActive(true);
            Invoke(nameof(CompletePanel), 1);
        }
    }

    void CompletePanel()
    {
        int lvlReward = flaskToComplete * 20;
        levelCompletebar.SetActive(true);
        UIManager.instance.gamePlayUIObj.SetActive(false);
        levelCompletebar.GetComponent<LevelCompletePanel>().originalRewardAmount = lvlReward;
        levelCompletebar.GetComponent<LevelCompletePanel>().starCounts = starsCount;
        CashManager.Instance.CashDebit(lvlReward);
        if (adsManager.Instance)
            adsManager.Instance.LogEvent("level_Complete" + currentLevel);
        if (adsManager.Instance)
        {
            adsManager.Instance.ShowInterstitialAd2();
        }

        if (currentLevel == PlayerPrefs.GetInt(MAX_UNLOCKED_LEVELS + gameType))
        {
            PlayerPrefs.SetInt(MAX_UNLOCKED_LEVELS + gameType, currentLevel + 1);
        }
    }
    public void Restart()
    {
        PlayerPrefs.SetInt("currentlevel", currentLevel);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);

    }
    public void NextLevel()
    {
        PlayerPrefs.SetInt("currentlevel", currentLevel + 1);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }
    bool shakeflask = false;
    bool hasfilledBottle = false;
    public void ResetFlaskSelection()
    {
        if (waterPouringAS.isPlaying)
            waterPouringAS.Stop();

        waterpouringImage.SetActive(false);
        if (shakeflask)
        {
            if (!shakesoundAS.isPlaying)
                shakesoundAS.Play();
            shakeflask = false;
            firstFlask.GetComponent<Transform>().DOShakeRotation(0.5f,30);
           //firstFlask.GetComponent<Transform>().DOShakePosition(0.25f);
        }

        hasfilledBottle = false;
        if(firstFlask)
        if(firstFlask)
        firstFlask.ResetPositionnRotaion();
        if(secondFlask)
        secondFlask.ResetPositionnRotaion();


        firstFlask = null;
        secondFlask = null;
        moveDone = false;

    }

    public void ClickSelect(Flask _flask)
    {

        if (firstFlask == null)
        {
            if (instructionTxtObj.activeSelf)
                instructionTxtObj.SetActive(false);

            firstFlask = _flask;
            firstFlask.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(firstFlask.GetComponent<RectTransform>().anchoredPosition.x, firstFlask.GetComponent<RectTransform>().anchoredPosition.y + 30);
            if (firstFlask.isthisflaskCompleted || firstFlask.FilledWaterLayer < 0)
            {
                //  print("this flask is already completed");
                shakeflask = true;
                ResetFlaskSelection();
                
                if (PlayerPrefs.GetInt("vibration", 1) == 1)
                    MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
            }
            /*             print("first flask is selected");*/
        }
        else if (firstFlask && secondFlask == null)
        {
            if (_flask != firstFlask)
            {
                secondFlask = _flask;
                secondFlask.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(secondFlask.GetComponent<RectTransform>().anchoredPosition.x, secondFlask.GetComponent<RectTransform>().anchoredPosition.y + 10);

                if ((firstFlask.IsEqualTo(firstFlask.GetCurrentFilledWaterColor(), secondFlask.GetCurrentFilledWaterColor()) && secondFlask.FilledWaterLayer < (secondFlask.waterLayer.Count - 1))
                    || secondFlask.FilledWaterLayer < 0)
                {
                    if (firstFlask.FilledWaterLayer >= 0)
                    {
                        if (!waterPouringAS.isPlaying) //if not playing sound...it should play
                            waterPouringAS.Play();

                        waterpouringImage.GetComponent<RectTransform>().anchoredPosition = secondFlask.GetComponent<RectTransform>().anchoredPosition + waterpouringOffset;
                        waterpouringImage.GetComponent<Image>().color = firstFlask.GetCurrentFilledWaterColor();

                        //change order of flask in heirarchy , so that it can be visible while pouring water
                        firstFlask.gameObject.transform.SetSiblingIndex(firstFlask.transform.parent.childCount - 1);
                        secondFlask.gameObject.transform.SetSiblingIndex(firstFlask.transform.parent.childCount - 2);

                        waterpouringImage.gameObject.transform.SetParent(firstFlask.transform.parent);
                        waterpouringImage.gameObject.transform.SetSiblingIndex(firstFlask.transform.parent.childCount - 3);



                        secondFlask.FillFlask(firstFlask.GetCurrentFilledWaterColor());
                        firstFlask.EmptyFlask(secondFlask.transform, offSet);
                        waterpouringImage.SetActive(true);
                        hasfilledBottle = true;
                        if (gameType == GameType.Movebase && !moveDone) //update moves
                        {
                            moveDone = true;
                            /* if (levelMoves > 0)
                             {*/
                            levelMoves += 1;
                            movesCounterTxt.text = levelMoves.ToString();
                            StarCalculations();
                            /*}
                            else
                            {
                                //game failed here
                            }*/
                        }
                    }
                    else
                    {
                       if(!hasfilledBottle)
                           shakeflask = true;

                        ResetFlaskSelection();
                       
                        if (PlayerPrefs.GetInt("vibration", 1) == 1)
                            MMVibrationManager.Haptic(HapticTypes.LightImpact);
                    }
                }
                else
                {
                    if (!hasfilledBottle)
                        shakeflask = true;
                    ResetFlaskSelection();
                
                    if (PlayerPrefs.GetInt("vibration", 1) == 1)
                        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
                }

            }
        }
    }

    public void AutoSelectBothFlask() //just to recheck both falsk
    {
        Flask flask1 = firstFlask;
        Flask flask2 = secondFlask;
        firstFlask = null;
        secondFlask = null;
        shakeflask = false;
        ClickSelect(flask1);
        ClickSelect(flask2);


    }
    //moves counter
    void StarCalculations()
    {
        // print("level moves" + levelMoves + " starts" + starsCount);
        if (levelMoves > thirdstar && starsCount == 2)
        {
            //  print("print" + starsCount);
            starsCount -= 1;
            GiveStar();
        }
        else if (levelMoves > secondstar && starsCount == 1)
        {
            starsCount -= 1;
            GiveStar();
        }

    }

    void GiveStar()
    {
        for (int i = 0; i <= stars.Length - 1; i++)
        {

            if (i <= starsCount)
            {
                stars[i].sprite = filledStar;
            }
            else
            {
                stars[i].sprite = unfilledStar;
            }
        }
    }
    void UpdateStarPrice()
    {
        onestar = currentLevelComponent.onestarThreshold;
        secondstar = currentLevelComponent.twostarThreshold;
        thirdstar = currentLevelComponent.threestarThreshold;
        stars[0].transform.GetChild(0).GetComponent<Text>().text = currentLevelComponent.onestarThreshold.ToString();
        stars[1].transform.GetChild(0).GetComponent<Text>().text = currentLevelComponent.twostarThreshold.ToString();
        stars[2].transform.GetChild(0).GetComponent<Text>().text = currentLevelComponent.threestarThreshold.ToString();

    }

    public void WatchVideoAddFlask() //wathc video to add flask
    {
        adsManager.Instance.Option = 3;
        adsManager.Instance.ShowRewardedVideoAd();
#if UNITY_EDITOR
        AddNewFlask();
#endif
    }
    public void AddNewFlask()
    {
        addflaskBtn.interactable = false;
        currentLevelComponent.emptyBottle.SetActive(true);
    }
}
