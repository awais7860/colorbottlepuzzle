﻿

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.

    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class Purchaser : MonoBehaviour, IStoreListener
{

        private static Purchaser _instance;

        public static string[] PriceList;
        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        // Product identifiers for all products capable of being purchased: 
        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

        // General product identifiers for the consumable, non-consumable, and subscription products.
        // Use these handles in the code to reference which product to purchase. Also use these values 
        // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
        // specific mapping to Unity Purchasing's AddProduct, below.
        public static string Removeads = "removead";
        public static string UnlockLevels = "unlocklevels";
        public static string UnlockWeapons = "unlockweapons";
        public static string UnlockALLGame = "unlockallgame";
        public static string coins1k = "coins1k";
        public static string coins10k = "coins10k";
        public static string coins20k = "coins20k";
       

       






    // Apple App Store-specific product identifier for the subscription product.
    private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

        // Google Play Store-specific product identifier subscription product.
        private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

    public static Purchaser instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Purchaser>();
                
            }
            return _instance;
        }


    }


        void Start()
        {
            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }

         
        }

        public void InitializePurchasing()
        {
            // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                // ... we are done here.
               GetPriceList();
                return;
            }

            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

           
            builder.AddProduct(Removeads, ProductType.NonConsumable);
            builder.AddProduct(UnlockLevels, ProductType.Consumable);
            builder.AddProduct(UnlockWeapons, ProductType.Consumable);
            builder.AddProduct(UnlockALLGame, ProductType.Consumable);
            builder.AddProduct(coins1k, ProductType.Consumable);
            builder.AddProduct(coins10k, ProductType.Consumable);
            builder.AddProduct(coins20k, ProductType.Consumable);
          
           
          







        //builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
        //    { kProductNameAppleSubscription, AppleAppStore.Name },
        //    { kProductNameGooglePlaySubscription, GooglePlay.Name },
        //});


        UnityPurchasing.Initialize(this, builder);
        }


        private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }


        public void BuyRemoveAd()
        {
            
            BuyProductID(Removeads);
        }

        public void UnlockLevel()
        {
            BuyProductID(UnlockLevels);
        }

    public void Buy1kCoins()
    {
        BuyProductID(coins1k);
    }

    public void Buy10kCoins()
    {
        BuyProductID(coins10k);
    }

    public void Buy20kCoins()
    {
        BuyProductID(coins20k);
    }

    public void BuyUnlockWeapon()
    {
        BuyProductID(UnlockWeapons);
    }

    public void BuyUnlockGame()
    {
        BuyProductID(UnlockALLGame);
    }

    public void GetPriceList(){
      
        PriceList=new string[7];
     
        if (m_StoreController != null && m_StoreController.products.WithID(Removeads) != null)
        {
            PriceList[0] = m_StoreController.products.WithID(Removeads).metadata.localizedPriceString;

        }
        else
            PriceList[0] = "Buy";

        if (m_StoreController != null && m_StoreController.products.WithID(coins1k) != null)
        {
            PriceList[1] = m_StoreController.products.WithID(coins1k).metadata.localizedPriceString;

        }
        else
            PriceList[1] = "Buy";

        if (m_StoreController != null && m_StoreController.products.WithID(coins10k) != null)
        {
            PriceList[2] = m_StoreController.products.WithID(coins10k).metadata.localizedPriceString;

        }
        else
            PriceList[2] = "Buy";

        if (m_StoreController != null && m_StoreController.products.WithID(coins20k) != null)
        {
            PriceList[3] = m_StoreController.products.WithID(coins20k).metadata.localizedPriceString;

        }
        else
            PriceList[3] = "Buy";

        if (m_StoreController != null && m_StoreController.products.WithID(UnlockALLGame) != null)
        {
            PriceList[4] = m_StoreController.products.WithID(UnlockALLGame).metadata.localizedPriceString;

        }
        else
            PriceList[4] = "Buy";

        if (m_StoreController != null && m_StoreController.products.WithID(UnlockLevels) != null)
        {
            PriceList[5] = m_StoreController.products.WithID(UnlockLevels).metadata.localizedPriceString;

        }
        else
            PriceList[5] = "Buy";

        if (m_StoreController != null && m_StoreController.products.WithID(UnlockWeapons) != null)
        {
            PriceList[6] = m_StoreController.products.WithID(UnlockWeapons).metadata.localizedPriceString;

        }
        else
            PriceList[6] = "Buy";





    }



    void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }


        //  
        // --- IStoreListener
        //

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            Debug.Log("OnInitialized: PASS");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
        }


        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }


        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            
                if (String.Equals(args.purchasedProduct.definition.id, Removeads, StringComparison.Ordinal))
                {
                    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
#if UNITY_EDITOR
            Debug.Log(" Thank you For Purchasing, Now Ad's are Removed");
#endif
            PlayerPrefs.SetInt("Removeads", 1);
           


                }
 
        else if (String.Equals(args.purchasedProduct.definition.id, UnlockWeapons, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

           // MainMenu.Instance.P_BuyAllGuns();
#if UNITY_EDITOR
            Debug.Log("Thank You for Purchasing");
#endif

        }

        else if (String.Equals(args.purchasedProduct.definition.id, UnlockALLGame, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

            PlayerPrefs.SetInt("Removeads", 1);
          //  MainMenu.Instance.P_BuyEveryThing();
#if UNITY_EDITOR
            Debug.Log("Thank You for Purchasing");
#endif

        }


        else if (String.Equals(args.purchasedProduct.definition.id, UnlockLevels, StringComparison.Ordinal))
                {
                    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

            //MainMenu.Instance.P_BuyLevels();
          
#if UNITY_EDITOR
            Debug.Log("All Levels Unlocked");
        #endif
          
                }

        else if (String.Equals(args.purchasedProduct.definition.id, coins1k, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

           // MainMenu.Instance.P_BuyCoins_1000();
#if UNITY_EDITOR
            Debug.Log("Thank You for Purchasing");
#endif

        }

        else if (String.Equals(args.purchasedProduct.definition.id, coins10k, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
           // MainMenu.Instance.P_BuyCoins_10000();


#if UNITY_EDITOR
            Debug.Log("Thank You for Purchasing");
#endif

        }
        else if (String.Equals(args.purchasedProduct.definition.id, coins20k, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

           /// MainMenu.Instance.P_BuyCoins_20000();

#if UNITY_EDITOR
            Debug.Log("Thank You for Purchasing");
#endif

        }

        else
                {
                            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
                        }

                        // Return a flag indicating whether this product has completely been received, or if the application needs 
                        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
                        // saving purchased products to the cloud, and when that save is delayed. 
                        return PurchaseProcessingResult.Complete;
                    }


        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }

    
    }
