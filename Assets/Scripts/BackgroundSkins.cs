﻿
using UnityEngine;
using UnityEngine.UI;

public class BackgroundSkins : MonoBehaviour
{
    public static BackgroundSkins instance;
    public int currentBackground;
    int maxUnlocked;
    public Button[] bgskinsButtons;
    public bool testSkins;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void OnEnable()
    {
        if (testSkins)
        {
            PlayerPrefs.SetInt("unlockedskins", currentBackground);
            PlayerPrefs.SetInt("currenbackground", currentBackground);
        }



        UpdateSkinLocks();
    }
    public void UpdateSkinLocks()
    {
        maxUnlocked = PlayerPrefs.GetInt("unlockedskins", 0);
        currentBackground = PlayerPrefs.GetInt("currenbackground", 0);
        for (int i = 0; i <= bgskinsButtons.Length - 1; i++)
        {
            if (i <= maxUnlocked)
            {
                bgskinsButtons[i].interactable = true;
                bgskinsButtons[i].transform.GetChild(2).gameObject.SetActive(false);
                if (currentBackground == i)
                    bgskinsButtons[i].transform.GetChild(1).gameObject.SetActive(true);
                else
                    bgskinsButtons[i].transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                bgskinsButtons[i].transform.GetChild(1).gameObject.SetActive(false);
                bgskinsButtons[i].interactable = false;

            }
        }
    }
    public void WatchVideo()
    {
        if (adsManager.Instance)
        { 
            adsManager.Instance.Option = 1;
            adsManager.Instance.ShowRewardedVideoAd();
        }

    }

   
    public void UnlockNewSkin()
    {
        if (CashManager.Instance && CashManager.Instance.CheckCash() > 200)
        {
            CashManager.Instance.CreditCash(200);
            PlayerPrefs.SetInt("unlockedskins", PlayerPrefs.GetInt("unlockedskins", 0) + 1);
            UpdateSkinLocks();
        }
        else {
            UIManager.instance.ShowMessage("Not have enough coins");
        }
    }
}