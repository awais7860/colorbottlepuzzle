﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public GameObject gamePlayUIObj, homeUIObj, levelCompleteObj, levelPanel, gameModepanel;
    public GameObject settingsPanel;
    public Image background;
    public Sprite[] bgSprites;
    public Text _messageTxt;
    public GameObject musicOnobj, musicOffobj;
    public GameObject vibrationOnobj, vibrationOffobj;
    public AudioSource mainMusic;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        gamePlayUIObj.SetActive(false);
        homeUIObj.SetActive(true);
        SetBackground(PlayerPrefs.GetInt("currenbackground", 0));
        if (PlayerPrefs.GetInt("music", 1) == 1)
        {
            musicOffobj.SetActive(false);
            musicOnobj.SetActive(true);
            mainMusic.Play();
        }
        else
        {
            musicOffobj.SetActive(true);
            musicOnobj.SetActive(false);
            mainMusic.Stop();
        }
        
        if (PlayerPrefs.GetInt("vibration", 1) == 1)
        {
            vibrationOffobj.SetActive(false);
            vibrationOnobj.SetActive(true);
        
        }
        else
        {
            vibrationOffobj.SetActive(true);
            vibrationOnobj.SetActive(false);

        }

    }
    public void SetBackground(int _bgNumber)
    {
        background.sprite = bgSprites[_bgNumber];
        PlayerPrefs.SetInt("currenbackground", _bgNumber);
        if (BackgroundSkins.instance)
            BackgroundSkins.instance.UpdateSkinLocks();
    }
    public void LoadNextLevel()
    {
        gamePlayUIObj.SetActive(true);
        homeUIObj.SetActive(false);
        GameManger.instance.TapToStart();
    }

    public void RestartGame()
    {
        if (adsManager.Instance)
            adsManager.Instance.ShowInterstitialAd2();
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void HomePanel()
    {
        gamePlayUIObj.SetActive(false);
        homeUIObj.SetActive(true);
        if (adsManager.Instance)
            adsManager.Instance.ShowInterstitialAd2();
    }
    public void ShowLevelsPanel(int _gameMode)
    {
        if (_gameMode == 0)
        {
            GameManger.instance.gameType = GameManger.GameType.Movebase;
        }
        else if (_gameMode == 1)
        {
            GameManger.instance.gameType = GameManger.GameType.timebase;
        }
        else
        {
            GameManger.instance.gameType = GameManger.GameType.both;
        }
        gameModepanel.SetActive(false);
        levelPanel.SetActive(true);
    }
    public void ToggleMusice()
    {
        if (musicOffobj.activeSelf)
        {
            musicOffobj.SetActive(false);
            musicOnobj.SetActive(true);
            PlayerPrefs.SetInt("music", 1);
            mainMusic.Play();
        }
        else
        {
            musicOffobj.SetActive(true);
            musicOnobj.SetActive(false);
            PlayerPrefs.SetInt("music", 0);
            mainMusic.Stop();
        }
    }
    public void ToggleVibration()
    {
        if (vibrationOffobj.activeSelf)
        {
            vibrationOffobj.SetActive(false);
            vibrationOnobj.SetActive(true);
            PlayerPrefs.SetInt("vibration", 1);
           
        }
        else
        {
            vibrationOffobj.SetActive(true);
            vibrationOnobj.SetActive(false);
            PlayerPrefs.SetInt("vibration", 0);
           

        }
    }
    public void HideLevelsPanel()
    {
        levelPanel.SetActive(false);
        gameModepanel.SetActive(true);
    }


    public void ShowSettings()
    {
        settingsPanel.SetActive(true);
    }
    public void ShowMessage(string _msg)
    {
        CancelInvoke(nameof(HideMessage));
        _messageTxt.transform.parent.gameObject.SetActive(true);
        _messageTxt.text = _msg;
        Invoke(nameof(HideMessage), 2);
    }
    void HideMessage()
    {
        _messageTxt.transform.parent.gameObject.SetActive(false);
    }
    public void MoreGames(string _url)
    {
        _url += Application.identifier;
        Application.OpenURL(_url);
    }
}
