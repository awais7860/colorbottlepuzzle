﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class Flask : MonoBehaviour
{
    public Color emptyColor;
    public List<GameObject> waterLayer;
    public float speed = 1;
    public int FilledWaterLayer = -1;
    public GameObject completeTag;
    // Start is called before the first frame update
    void Start()
    {
        orignalPosition = gameObject.GetComponent<RectTransform>().anchoredPosition;
        originalRotaion = gameObject.GetComponent<RectTransform>().rotation;

        for (int i = 0; i <= waterLayer.Count - 1; i++)
        {
            if (!waterLayer[i].GetComponent<WaterLayer>().filled)
            {
                FilledWaterLayer = i - 1;
                break;
            }
            else
            {
                FilledWaterLayer = i;
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            CheckifFilled();
            GameManger.instance.CheckLevelComplete();
        }
    }


    public void SelectFlask()
    {
        GameManger.instance.ClickSelect(this);
    }

    Vector2 orignalPosition;
    Quaternion originalRotaion;

    public void EmptyFlask(Transform pouringPoint, Vector2 _offset)
    {
        gameObject.GetComponent<RectTransform>().anchoredPosition = pouringPoint.GetComponent<RectTransform>().anchoredPosition + _offset;
        gameObject.GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, -90);
        Image child = waterLayer[FilledWaterLayer].GetComponent<Image>();
        child.DOFillAmount(0, 1).SetEase(Ease.Linear).OnComplete(LoopFill);
        FilledWaterLayer--;
        /* print("flask 1 filled layer" + FilledWaterLayer);*/
       // RotatePouringFlaskSlowly();
    }

    void RotatePouringFlaskSlowly()
    {
        gameObject.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, -90),1.5f);
    }
    public void LoopFill()
    {
        GameManger.instance.AutoSelectBothFlask();
        FlaskComplitionCheck();
    }
    public void ResetPositionnRotaion()
    {
        gameObject.GetComponent<RectTransform>().anchoredPosition = orignalPosition;
        gameObject.GetComponent<RectTransform>().rotation = originalRotaion;

        FlaskComplitionCheck();
    }

    public void FillFlask(Color fillColor)
    {
        FilledWaterLayer++;
        if (FilledWaterLayer <= waterLayer.Count - 1)
        {
            Image child = waterLayer[FilledWaterLayer].GetComponent<Image>();
            child.fillAmount = 0;
            child.color = fillColor;
            child.GetComponent<WaterLayer>().filled = true;
            child.DOFillAmount(1, 1).SetEase(Ease.Linear).OnComplete(ResetPositionnRotaion);

        }
        else
        {
            FlaskComplitionCheck();
        }

    }

    public Color GetCurrentFilledWaterColor()
    {
        if (FilledWaterLayer < 0)
        {
            return waterLayer[0].GetComponent<Image>().color;
        }
        else
        {
            return waterLayer[FilledWaterLayer].GetComponent<Image>().color;
        }

    }

    void FlaskComplitionCheck()
    {

        CheckifFilled();
        GameManger.instance.CheckLevelComplete();
    }

    [HideInInspector] public bool isthisflaskCompleted = false;
    void CheckifFilled() //check if this flask is filled
    {
        if (isthisflaskCompleted)
        {
            return;
        }

        if (FilledWaterLayer == waterLayer.Count - 1)
        {
            for (int i = 0; i < waterLayer.Count - 1; i++)
            {
                if (IsEqualTo(waterLayer[i].GetComponent<Image>().color, waterLayer[i + 1].GetComponent<Image>().color))
                {
                    isthisflaskCompleted = true;
                }
                else
                {
                    isthisflaskCompleted = false;
                    break;
                }
            }
        }

        if (isthisflaskCompleted)
        {
            GameManger.instance.completedFlask++;
            if (completeTag)
            { 
                completeTag.SetActive(true);
                GameManger.instance.bottleCompletePaticles.SetActive(true);
                Invoke(nameof(HideBottleParticle),3);
            }
        }
    }

    void HideBottleParticle()
    {
        GameManger.instance.bottleCompletePaticles.SetActive(false);
    }
    public bool IsEqualTo(Color me, Color other)
    {
        return me.r == other.r && me.g == other.g && me.b == other.b && me.a == other.a;
    }
}

