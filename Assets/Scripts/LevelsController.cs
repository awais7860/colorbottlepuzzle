﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsController : MonoBehaviour
{
    public Button[] levelButtons;
    public const string MAX_UNLOCKED_LEVELS = "maxunlockedlevel";
    public const string CURRENT_LEVEL = "currentlevel";
    int currentLevel;
   public int maxUnlockedlevel;
    public bool testLevels;
    // Start is called before the first frame update
    void OnEnable()
    {
        if (testLevels)
        {
            PlayerPrefs.SetInt(MAX_UNLOCKED_LEVELS + GameManger.instance.gameType,maxUnlockedlevel);
        }
        UpdateLevelLocks();
        //current level of select  
    //  update
    }

    public void UpdateLevelLocks()
    {
        currentLevel = PlayerPrefs.GetInt(CURRENT_LEVEL + GameManger.instance.gameType);
        maxUnlockedlevel = PlayerPrefs.GetInt(MAX_UNLOCKED_LEVELS + GameManger.instance.gameType);

        for (int i = 0; i <= levelButtons.Length - 1; i++)
        {
            if (i <= maxUnlockedlevel)
            {
                levelButtons[i].interactable = true;
                levelButtons[i].transform.GetChild(0).gameObject.GetComponent<Text>().text=(i+1).ToString();
                levelButtons[i].transform.GetChild(2).gameObject.SetActive(false);
                if (currentLevel == i)
                    levelButtons[i].transform.GetChild(1).gameObject.SetActive(true);
                else
                    levelButtons[i].transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                levelButtons[i].transform.GetChild(1).gameObject.SetActive(false);
                levelButtons[i].interactable = false;

            }
        }
    }
    /*
        // Update is called once per frame
        void Update()
        {

        }*/
}
