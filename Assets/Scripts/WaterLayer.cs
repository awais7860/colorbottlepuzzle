﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*[ExecuteInEditMode]*/
public class WaterLayer : MonoBehaviour
{
    public bool filled = false;
    public enum ColorType
    {
        Empty = -1, black, red, green, yellow,
        white, blue, purpul, pink, gray, silver,
        olive, aqua, teel, lime, marron, orange,brown
    };
    public int myColorNumber = -1;
    public ColorType myColor;

    Image image;
    private void Awake()
    {
        image = gameObject.GetComponent<Image>();
        AssignColor(myColor);
    }
    /*// Start is called before the first frame update
    void Start()
    {
      
       
    }*/

    public void AssignColor(ColorType _color)
    {
        if (_color == ColorType.Empty)
        {
            image.color = new Color(1, 1, 1, 0);
            filled = false;
        }
        else if (_color == ColorType.black)
        {
            image.color = Color.black;
        }
        else if (_color == ColorType.green)
        {
            image.color = Color.green;
        }
        else if (_color == ColorType.yellow)
        {
            image.color = Color.yellow;
        }
        else if (_color == ColorType.red)
        {
            image.color = Color.red;
        }
        else if (_color == ColorType.blue)
        {
            image.color = Color.blue;
        }
        else if (_color == ColorType.gray)
        {
            image.color = Color.gray;
        }
        else if (_color == ColorType.orange)
        {
            image.color = GameManger.instance.orange;
        }
        else if (_color == ColorType.olive)
        {
            image.color = GameManger.instance.olive;
        }
        else if (_color == ColorType.purpul)
        {
            image.color = GameManger.instance.purple;
        }
        else if (_color == ColorType.pink)
        {
            image.color = GameManger.instance.pink;
        }
        else if (_color == ColorType.silver)
        {
            image.color = GameManger.instance.silver;
        }
        else if (_color == ColorType.lime)
        {
            image.color = GameManger.instance.lime;
        }
        else if (_color == ColorType.aqua)
        {
            image.color = GameManger.instance.aqua;
        }
        else if (_color == ColorType.marron)
        {
            image.color = GameManger.instance.marron;
        }
        else if (_color == ColorType.teel)
        {
            image.color = GameManger.instance.teel;
        }else if (_color == ColorType.brown)
        {
            image.color = GameManger.instance.brown;
        }

        if (myColor == ColorType.Empty)
        {
            filled = false;
        }
        else
        {
            filled = true;
        }

        myColor = _color;
    }

}
