﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSettings : MonoBehaviour
{
   
   // public int totalMoves;
    public int threestarThreshold; //threshold for moves
    public int twostarThreshold;
    public int onestarThreshold;
    public float levelTime;
    public int bottleTofill;
    public GameObject emptyBottle;
}
