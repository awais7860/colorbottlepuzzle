﻿
using UnityEngine;
using System.Collections;
using System.Threading.Tasks;



public class ADCalling : MonoBehaviour
{

    #region Instance

    private static ADCalling _instance;

    public static ADCalling Instance
    {
        get
        {

            if (_instance == null)
            {
#if UNITY_ANDROID
                _instance = GameObject.FindObjectOfType<ADCalling>();
                DontDestroyOnLoad(_instance.gameObject);
#endif
            }
            return _instance;

        }
    }

    void Awake()
    {
        #region Instance assigning
#if UNITY_ANDROID

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {

            if (this != _instance)
                Destroy(this.gameObject);
        }

#endif
        #endregion
    }

    #endregion

    #region Main AD
    public void AdmobBanner()
    {


        // if ((PlayerPrefs.GetString("Admobbanner") == "true") || PlayerPrefs.GetString("Admobbanner") == "True")
        //  adsManager.Instance.ShowBannerAd();

    }

    public void FbBanner()
    {

        //  if ((PlayerPrefs.GetString("FacebookBanner") == "true") || PlayerPrefs.GetString("FacebookBanner") == "True")
        //  AdsManager.Instance.ShowFbBanner();
    }

    public void MainPanel()
    {
        // if ((PlayerPrefs.GetString("MainMenu") == "true") || PlayerPrefs.GetString("MainMenu") == "True")
        // AdsManager.Instance.ShowInterstitail();

        //  AdsManager.Instance.LogEvent("MainMenuPanel");

    }

    public void SettingPanel()
    {
        adsManager.Instance.HideBannerAd();
        adsManager.Instance.ShowInterstitialAd2();
        adsManager.Instance.ShowRectBannerAd();
    }

    public void ModePanel()
    {
        //  NativeModePanel();
    }



    public void GameWinPanel()
    {
        adsManager.Instance.HideBannerAd();
        adsManager.Instance.ShowInterstitialAd2();
        adsManager.Instance.ShowRectBannerAd();
    }

    public void GameFailPanel()
    {

        adsManager.Instance.HideBannerAd();
        adsManager.Instance.ShowInterstitialAd2();
        adsManager.Instance.ShowRectBannerAd();
    }

    public void GamePausePanel()
    {
        //adsManager.Instance.HideBannerAd();
      //  adsManager.Instance.ShowUnityAd();
        adsManager.Instance.ShowRectBannerAd();

    }

    public void ExitPanel()
    {
        adsManager.Instance.ShowRectBannerAd();
    }


    #endregion

    #region Native Ad


    #endregion

    #region Back Buttons
    public void MenuBack()
    {
        adsManager.Instance.HideRectBannerAd();
    }

    public void Resume()
    {

        adsManager.Instance.HideRectBannerAd();
        //  adsManager.Instance.ShowBannerAd();
    }

    public void Home()
    {
        adsManager.Instance.HideRectBannerAd();

    }
    #endregion
}
