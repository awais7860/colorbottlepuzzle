﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{

    #region instances
    public Text Mints, Secs;
    float _Timer;
    static float mint = 0, sec = 0;
    public static float speedLimit;
    public float Timer
    {
        get { return _Timer; }
        set { _Timer = value; }
    }
    // public GUIManagerGP obj;
    #endregion

    #region core
    void Start()
    {

        Timer = 120;
        mint = 0; sec = 0;
    }
    float updateTime=0;
    // Update is called once per frame
    void Update()
    {
        if (Timer > 0 && updateTime<Time.time)
        {
            updateTime = Time.time + (Time.deltaTime*1);
            Timer -= Time.deltaTime;

            // = Mathf.Floor(timer % 60).ToString("00");

            Mints.text = Mathf.Floor(Timer / 60).ToString("00") + " :";
            Secs.text = Mathf.Floor(Timer % 60).ToString("00");
            mint = Mathf.Floor(Timer / 60);
            sec = Mathf.Floor(Timer % 60);
        }
        else
        {
          //  TimeCountModeManager.Instance.ShowResultsOnTimeEnd();
           
        }


    }
    #endregion

    #region calculate bounus
    public int Bounus()
    {


        int bounus = 0;

        if (mint >= 0 || sec >= 0)
        {

            for (; mint >= 0;)
            {

                mint -= 1;
                bounus += 5;

            }

            for (; sec >= 0;)
            {

                sec -= 1;
                bounus += 5;

            }
        }
        return bounus;
    }
    #endregion
}

