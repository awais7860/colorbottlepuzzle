﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BottleSkins : MonoBehaviour
{

    public static BottleSkins instance;
    public int currentbottleskins;
    int maxUnlocked;
    public Button[] bottleskinsButtons;
    public bool testSkins;
    public GameObject comingsoonText;
    private void Awake()
    {
        instance = this;
        comingsoonText.SetActive(false);
    }
    // Start is called before the first frame update
    void OnEnable()
    {
        if (testSkins)
        {
            PlayerPrefs.SetInt("unlockedbottleskins", currentbottleskins);
            PlayerPrefs.SetInt("currenbottleskins", currentbottleskins);
        }



        UpdateSkinLocks();
    }
    public void UpdateSkinLocks()
    {
        maxUnlocked = PlayerPrefs.GetInt("unlockedbottleskins", 0);
        currentbottleskins = PlayerPrefs.GetInt("currenbottleskins", 0);
        for (int i = 0; i <= bottleskinsButtons.Length - 1; i++)
        {
            if (i <= maxUnlocked)
            {
                bottleskinsButtons[i].interactable = true;
                bottleskinsButtons[i].transform.GetChild(2).gameObject.SetActive(false);
                if (currentbottleskins== i)
                    bottleskinsButtons[i].transform.GetChild(1).gameObject.SetActive(true);
                else
                    bottleskinsButtons[i].transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                bottleskinsButtons[i].transform.GetChild(1).gameObject.SetActive(false);
                bottleskinsButtons[i].interactable = false;

            }
        }
    }
    public void WatchVideo()
    {
        if (adsManager.Instance)
        {
            adsManager.Instance.Option = 2;
            adsManager.Instance.ShowRewardedVideoAd();
        }

    }


    public void UnlockNewSkin()
    {
        if (CashManager.Instance && CashManager.Instance.CheckCash() > 200)
        {
            CashManager.Instance.CreditCash(200);
            PlayerPrefs.SetInt("unlockedbottleskins", PlayerPrefs.GetInt("unlockedbottleskins", 0) + 1);
            UpdateSkinLocks();
        }
        else
        {
            UIManager.instance.ShowMessage("Not have enough coins");
        }
    }
}
